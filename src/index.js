/**
 * @module index1.js
 */
import { Chart, Tooltip, Axis, Bar } from 'viser-react';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Checkbox, Row, Col} from 'antd';
import mloAlg from './mloAlg'
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const ANSWER_COUNT = 165;
const COL_COUNT = 15;
const ROW_COUNT = 11;
const PLUS = '+';
const MINUS = '-';

class Answer extends Component {
    render() {
        return (
            <div onKeyPress={this.props.onKeyPress}>
                <Checkbox
                    ref={(checkbox) => {
                        this.props.refs[this.props.value] = checkbox
                    }}
                    autoFocus={this.props.value === 0}
                    onClick={this.props.onClick}
                    checked={this.props.checked}>
                    {this.props.value + 1}
                </Checkbox>
            </div>
        )
    }
}

class Blank extends Component {
    renderAnswer(answerKey) {
        return (
            <Answer
                onKeyPress={(event) => this.props.onKeyPress(event, answerKey)}
                onClick={() => this.props.onClick(answerKey)}
                checked={this.props.answers[answerKey]}
                refs={this.props.refs}
                value={answerKey}
            />
        )
    }

    render() {
        let answerKey = 0;
        return new Array(ROW_COUNT).fill('').map((_, rowKey) => {
            const cols = new Array(COL_COUNT).fill('').map((_, colKey) => {
                return (
                    <Col span={2} key={'col' + colKey}>
                        {this.renderAnswer(answerKey++)}
                    </Col>
                );

            });
            return (
                <Row key={'row' + rowKey}>
                    {cols}
                </Row>
            )
        })
    }
}

class Result extends Component {
    renderCol(value) {
        return (
            <Col span={1}>
                {value}
            </Col>
        )
    }

    render() {
        const rawData = this.props.rawData;
        const correctedData = this.props.correctedData;
        const tData = this.props.tData;
        return (
            <div>
                <Row>
                    <Col span={4}/>
                    {this.renderCol('L')}
                    {this.renderCol('F')}
                    {this.renderCol('K')}
                    {this.renderCol('Hs')}
                    {this.renderCol('D')}
                    {this.renderCol('Hy')}
                    {this.renderCol('Pd')}
                    {this.renderCol('Mf')}
                    {this.renderCol('Pa')}
                    {this.renderCol('Pt')}
                    {this.renderCol('Sc')}
                    {this.renderCol('Ma')}
                    {this.renderCol('Si')}
                </Row>
                <Row>
                    <Col span={4}>{"Сырые баллы"}</Col>
                    {this.renderCol(rawData.lValue)}
                    {this.renderCol(rawData.fValue)}
                    {this.renderCol(rawData.kValue)}
                    {this.renderCol(rawData.hsValue)}
                    {this.renderCol(rawData.dValue)}
                    {this.renderCol(rawData.hyValue)}
                    {this.renderCol(rawData.pdValue)}
                    {this.renderCol(rawData.mfValue)}
                    {this.renderCol(rawData.paValue)}
                    {this.renderCol(rawData.ptValue)}
                    {this.renderCol(rawData.scValue)}
                    {this.renderCol(rawData.maValue)}
                    {this.renderCol(rawData.siValue)}
                </Row>
                <Row>
                    <Col span={4}>{"Откорректированные баллы"}</Col>
                    {this.renderCol(correctedData.lValue)}
                    {this.renderCol(correctedData.fValue)}
                    {this.renderCol(correctedData.kValue)}
                    {this.renderCol(correctedData.hsValue)}
                    {this.renderCol(correctedData.dValue)}
                    {this.renderCol(correctedData.hyValue)}
                    {this.renderCol(correctedData.pdValue)}
                    {this.renderCol(correctedData.mfValue)}
                    {this.renderCol(correctedData.paValue)}
                    {this.renderCol(correctedData.ptValue)}
                    {this.renderCol(correctedData.scValue)}
                    {this.renderCol(correctedData.maValue)}
                    {this.renderCol(correctedData.siValue)}
                </Row>
                <Row>
                    <Col span={4}>{"Т баллы"}</Col>
                    {this.renderCol(tData.lValue)}
                    {this.renderCol(tData.fValue)}
                    {this.renderCol(tData.kValue)}
                    {this.renderCol(tData.hsValue)}
                    {this.renderCol(tData.dValue)}
                    {this.renderCol(tData.hyValue)}
                    {this.renderCol(tData.pdValue)}
                    {this.renderCol(tData.mfValue)}
                    {this.renderCol(tData.paValue)}
                    {this.renderCol(tData.ptValue)}
                    {this.renderCol(tData.scValue)}
                    {this.renderCol(tData.maValue)}
                    {this.renderCol(tData.siValue)}
                </Row>
            </div>
        )
    }
}

class ResulChart extends Component{

    getScale()
    {
        return [{
            dataKey: 'value',
            tickInterval: 0,
        }];
    }


    render(){
        const tData = this.props.tData;
        const data = [
            { name: 'L', value: tData.lValue },
            { name: 'F', value: tData.fValue },
            { name: 'K', value: tData.kValue },
            { name: 'Hs', value: tData.hsValue },
            { name: 'D', value: tData.dValue },
            { name: 'Hy', value: tData.hyValue },
            { name: 'Pd', value: tData.pdValue },
            { name: 'Mf', value: tData.mfValue },
            { name: 'Pa', value: tData.paValue },
            { name: 'Pt', value: tData.ptValue },
            { name: 'Sc', value: tData.scValue },
            { name: 'Ma', value: tData.maValue },
            { name: 'Si', value: tData.siValue },
        ];

        return (
            <Chart forceFit height={400} data={data} scale={this.getScale}>
                <Tooltip />
                <Axis />
                <Bar position="name*value" />
            </Chart>
        )
    }
}

class Board extends Component {
    constructor(props) {
        super(props);
        this.checkbox = [];
        this.state = {
            rawData:
                {
                    lValue: 0,
                    fValue: 0,
                    kValue: 0,
                    hsValue: 0,
                    dValue: 0,
                    hyValue: 0,
                    pdValue: 0,
                    mfValue: 0,
                    paValue: 0,
                    ptValue: 0,
                    scValue: 0,
                    maValue: 0,
                    siValue: 0,
                },
            correctedData:
                {
                    lValue: 0,
                    fValue: 0,
                    kValue: 0,
                    hsValue: 0,
                    dValue: 0,
                    hyValue: 0,
                    pdValue: 0,
                    mfValue: 0,
                    paValue: 0,
                    ptValue: 0,
                    scValue: 0,
                    maValue: 0,
                    siValue: 0,
                },
            tData:
                {
                    lValue: 0,
                    fValue: 0,
                    kValue: 0,
                    hsValue: 0,
                    dValue: 0,
                    hyValue: 0,
                    pdValue: 0,
                    mfValue: 0,
                    paValue: 0,
                    ptValue: 0,
                    scValue: 0,
                    maValue: 0,
                    siValue: 0,
                },
            answers: new Array(ANSWER_COUNT).fill(false),
        };
    }

    handleKeyPress(event, answerKey) {
        const key = event.key;
        console.log(key);
        let keyValue = null;
        if (key === PLUS) {

            keyValue = true;
        } else if (key === MINUS) {
            keyValue = false;

        } else if (key === 'Delete' && answerKey > 0) {
            this.checkbox[answerKey - 1].focus();
            return;
        }
        if (keyValue === null) {
            return;
        }

        this.handleInput(keyValue, answerKey)
    }

    handleClick(answerKey) {
        this.handleInput(!this.state.answers[answerKey], answerKey)
    }

    handleInput(value, answerKey) {
        const answers = this.state.answers.slice();
        answers[answerKey] = value;

        const data = mloAlg.calculate(answers);
        this.setState({

            rawData: data.rawData,
            correctedData: data.correctedData,
            tData: data.tData,
            answers: answers,
        });

        let focusIndex = answerKey + 1;
        if (focusIndex < ANSWER_COUNT) {
            this.checkbox[focusIndex].focus();
        } else {
            this.checkbox[0].focus();
        }
    }

    render() {
        return (
            <Row>
                <Col offset="1" span={22}>
                    <Blank
                        refs={this.checkbox}
                        answers={this.state.answers}
                        onKeyPress={(event, answerKey) => this.handleKeyPress(event, answerKey)}
                        onClick={(answerKey) => this.handleClick(answerKey)}
                    />
                </Col>
                <Col offset="1" span={21}>
                    <Result
                        rawData={this.state.rawData}
                        correctedData={this.state.correctedData}
                        tData={this.state.tData}
                    />
                </Col>
                <Col offset="1" span={15}>
                    <ResulChart
                        tData={this.state.tData}
                    />
                </Col>
            </Row>
        );
    }
}

ReactDOM.render(<Board/>, document.getElementById('root'));
registerServiceWorker();