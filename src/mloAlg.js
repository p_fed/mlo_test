const L_ID = {
    trueIds: [],
    falseIds: [0, 9, 30, 68, 77, 91, 100, 115, 127, 147],
};
const F_ID = {
    trueIds: [3, 7, 10, 17, 19, 21, 36, 40, 46, 59, 71, 81, 83, 85, 90, 95, 97, 102, 114, 152,],
    falseIds: [1, 24, 42, 43, 52,],
};
const K_ID = {
    trueIds: [34,],
    falseIds: [14, 45, 47, 63, 72, 89, 101, 150,],
};
const HS_ID = {
    trueIds: [16, 66,],
    falseIds: [1, 2, 4, 22, 37, 52, 54, 57, 61, 74, 92,],
};
const D_ID = {
    trueIds: [15, 16, 29, 38, 45,],
    falseIds: [4, 13, 22, 25, 26, 31, 33, 49, 51, 52, 53, 54, 66, 67, 76, 101,],
};
const HY_ID = {
    trueIds: [10, 16, 19, 20, 27, 64, 66,],
    falseIds: [1, 2, 22, 32, 37, 41, 44, 47, 52, 57, 60, 61, 63, 74, 87, 89, 94, 96, 98,],
};
const PD_ID = {
    trueIds: [5, 7, 10, 11, 13, 40, 41, 55, 71, 80, 81, 90, 113,],
    falseIds: [12, 34, 44, 47, 54, 78, 89, 96, 99, 101,],
};
const MF_ID = {
    trueIds: [62, 65, 72,],
    falseIds: [8, 42, 49, 73, 85, 86,],
};
const PA_ID = {
    trueIds: [3, 6, 7, 9, 17, 38, 42, 45, 47, 97, 103, 124, 149, 151,],
    falseIds: [32, 41, 83, 136, 144, 154,],
};
const PT_ID = {
    trueIds: [6, 9, 10, 15, 27, 29, 36, 40, 66, 72, 79, 87, 102, 103, 109, 116, 119, 121, 122,],
    falseIds: [1, 51,],
};
const SC_ID = {
    trueIds: [3, 5, 6, 7, 9, 10, 11, 13, 15, 20, 23, 35, 38, 55, 59, 62, 69, 79, 88, 97, 102, 104, 105, 107, 110, 118, 122, 123,],
    falseIds: [12, 37, 43, 65, 106,],
};
const MA_ID = {
    trueIds: [5, 6, 26, 35, 41, 48, 55, 58, 75, 76, 79, 88, 89, 92, 94,],
    falseIds: [39, 42, 63, 95,],
};
const SI_ID = {
    trueIds: [63, 84, 125, 159, 162,],
    falseIds: [11, 48, 73, 89, 143, 146, 158,],
};

exports.calculate = (answers) => {
    const rawData = {
        lValue: calc(L_ID, answers),
        fValue: calc(F_ID, answers),
        kValue: calc(K_ID, answers),
        hsValue: calc(HS_ID, answers),
        dValue: calc(D_ID, answers),
        hyValue: calc(HY_ID, answers),
        pdValue: calc(PD_ID, answers),
        mfValue: calc(MF_ID, answers),
        paValue: calc(PA_ID, answers),
        ptValue: calc(PT_ID, answers),
        scValue: calc(SC_ID, answers),
        maValue: calc(MA_ID, answers),
        siValue: calc(SI_ID, answers),
    };

    const correctedData = calculateCorrectedData(Object.assign({}, rawData));
    const tData = calculateTData(Object.assign({}, correctedData));

    return {
        rawData: rawData,
        correctedData: correctedData,
        tData: tData
    }

};

const calculateCorrectedData = (rawData) => {
    const kValue = rawData.kValue;
    const hsValue = rawData.hsValue;
    const pdValue = rawData.pdValue;
    const ptValue = rawData.ptValue;
    const scValue = rawData.scValue;
    const maValue = rawData.maValue;

    rawData.hsValue = Math.round(kValue * 0.5 + hsValue);
    rawData.pdValue = Math.round(kValue * 0.4 + pdValue);
    rawData.ptValue = Math.round(kValue + ptValue);
    rawData.scValue = Math.round(kValue + scValue);
    rawData.maValue = Math.round(kValue * 0.2 + maValue);
    return rawData;
};

const calculateTData = (correctedRawData) => {
    return {
        lValue: calcLValue(correctedRawData.lValue),
        fValue: calcFValue(correctedRawData.fValue),
        kValue: calcKValue(correctedRawData.kValue),
        hsValue: calcHsValue(correctedRawData.hsValue),
        dValue: calcDValue(correctedRawData.dValue),
        hyValue: calcHyValue(correctedRawData.hyValue),
        pdValue: calcPdValue(correctedRawData.pdValue),
        mfValue: calcMfValue(correctedRawData.mfValue),
        paValue: calcPaValue(correctedRawData.paValue),
        ptValue: calcPtValue(correctedRawData.ptValue),
        scValue: calcScValue(correctedRawData.scValue),
        maValue: calcMaValue(correctedRawData.maValue),
        siValue: calcSiValue(correctedRawData.siValue),
    };
};

const calcLValue = (lValue) => {
    if (lValue <= 3) {
        return 4 * lValue + 35;
    }
    if (lValue <= 7) {
        return 4 * (lValue - 4) + 52;
    }
    if (lValue === 8) {
        return 67;
    }
    if (lValue <= 11) {
        return 5 * (lValue - 9) + 71;
    }
    if (lValue <= 14) {
        return 7 * (lValue - 12) + 87;
    }
    if (lValue <= 15) {
        return 107;
    }
    return 0;
};

const calcFValue = (fValue) => {
    if (fValue <= 11) {
        return 3 * fValue + 32;
    }
    if (fValue <= 13) {
        return 4 * (fValue - 12) + 69;
    }
    if (fValue <= 25) {
        return 3 * (fValue - 14) + 76;
    }
    return 0;
};

const calcKValue = (kValue) => {
    if (kValue === 0) {
        return 44;
    }
    if (kValue <= 8) {
        return 5 * (kValue - 1) + 50;
    }
    if (kValue === 9) {
        return 91;
    }
    return 0;
};

const calcHsValue = (hsValue) => {
    if (hsValue === 0) {
        return 37;
    }
    if (hsValue === 1) {
        return 40;
    }
    if (hsValue <= 5) {
        return 2 * (hsValue - 2) + 45;
    }
    if (hsValue <= 8) {
        return 3 * (hsValue - 5) + 51;
    }
    if (hsValue <= 18) {
        return 5 * (hsValue - 8) + 60;
    }
    return 0;
};

const calcDValue = (dValue) => {
    if (dValue === 0) {
        return 35;
    }
    if (dValue <= 5) {
        return 3 * (dValue - 1) + 39;
    }
    if (dValue <= 7) {
        return 2 * (dValue - 6) + 56;
    }
    if (dValue <= 9) {
        return 2 * (dValue - 8) + 63;
    }
    if (dValue <= 11) {
        return 4 * (dValue - 10) + 70;
    }
    if (dValue <= 19) {
        return 3 * (dValue - 12) + 77;
    }
    if (dValue <= 21) {
        return 5 * (dValue - 20) + 105;
    }
    return 0;
};

const calcHyValue = (hyValue) => {
    if (hyValue <= 5) {
        return 3 * (hyValue) + 30;
    }
    if (hyValue <= 7) {
        return 2 * (hyValue - 6) + 47;
    }
    if (hyValue <= 10) {
        return 2 * (hyValue - 8) + 52;
    }
    if (hyValue <= 12) {
        return 2 * (hyValue - 11) + 60;
    }
    if (hyValue <= 14) {
        return 3 * (hyValue - 13) + 65;
    }
    if (hyValue <= 16) {
        return 3 * (hyValue - 15) + 70;
    }
    if (hyValue <= 18) {
        return 3 * (hyValue - 17) + 75;
    }
    if (hyValue <= 20) {
        return 3 * (hyValue - 19) + 82;
    }
    if (hyValue <= 22) {
        return 6 * (hyValue - 21) + 89;
    }
    if (hyValue <= 24) {
        return 5 * (hyValue - 23) + 100;
    }
    if (hyValue <= 26) {
        return 7 * (hyValue - 25) + 111;
    }
    return 0;
};

const calcPdValue = (pdValue) => {
    if (pdValue <= 1) {
        return 5 * (pdValue) + 30;
    }
    if (pdValue <= 3) {
        return 4 * (pdValue - 2) + 38;
    }
    if (pdValue <= 5) {
        return 3 * (pdValue - 4) + 45;
    }
    if (pdValue <= 7) {
        return 3 * (pdValue - 6) + 52;
    }
    if (pdValue <= 9) {
        return 2 * (pdValue - 8) + 60;
    }
    if (pdValue <= 12) {
        return 2 * (pdValue - 10) + 65;
    }
    if (pdValue <= 25) {
        return 3 * (pdValue - 13) + 72;
    }
    if (pdValue <= 27) {
        return 4 * (pdValue - 26) + 112;
    }
    if (pdValue === 28) {
        return 119;
    }
    return 0;
};

const calcMfValue = (mfValue) => {
    if (mfValue <= 4) {
        return 5 * (mfValue) + 35;
    }
    if (mfValue <= 6) {
        return 4 * (mfValue - 5) + 59;
    }
    if (mfValue <= 8) {
        return 7 * (mfValue - 7) + 71;
    }
    if (mfValue === 9) {
        return 88;
    }
    return 0;
};

const calcPaValue = (paValue) => {
    if (paValue <= 1) {
        return 4 * (paValue) + 27;
    }
    if (paValue <= 3) {
        return 4 * (paValue - 2) + 36;
    }
    if (paValue <= 9) {
        return 3 * (paValue - 4) + 43;
    }
    if (paValue <= 12) {
        return 4 * (paValue - 10) + 63;
    }
    if (paValue <= 14) {
        return 4 * (paValue - 13) + 74;
    }
    if (paValue <= 17) {
        return 3 * (paValue - 15) + 81;
    }
    if (paValue <= 19) {
        return 4 * (paValue - 18) + 91;
    }
    if (paValue === 20) {
        return 100;
    }
    return 0;
};

const calcPtValue = (ptValue) => {
    if (ptValue <= 1) {
        return 2 * (ptValue) + 35;
    }
    if (ptValue <= 7) {
        return 3 * (ptValue - 2) + 40;
    }
    if (ptValue <= 13) {
        return 2 * (ptValue - 8) + 57;
    }
    if (ptValue <= 18) {
        return (ptValue - 14) + 70;
    }
    if (ptValue <= 21) {
        return (ptValue - 19) + 76;
    }
    if (ptValue <= 23) {
        return 3 * (ptValue - 22) + 81;
    }
    if (ptValue <= 25) {
        return 2 * (ptValue - 24) + 86;
    }
    if (ptValue <= 27) {
        return 2 * (ptValue - 26) + 91;
    }
    if (ptValue <= 30) {
        return 4 * (ptValue - 28) + 97;
    }
    return 0;
};

const calcScValue = (scValue) => {
    if (scValue <= 3) {
        return 4 * (scValue) + 27;
    }
    if (scValue <= 7) {
        return 3 * (scValue - 4) + 42;
    }
    if (scValue <= 41) {
        return 2 * (scValue - 8) + 53;
    }
    if (scValue <= 42) {
        return 120;
    }

    return 0;
};

const calcMaValue = (maValue) => {
    if (maValue <= 3) {
        return 3 * (maValue) + 33;
    }
    if (maValue <= 6) {
        return 3 * (maValue - 4) + 47;
    }
    if (maValue <= 8) {
        return 2 * (maValue - 7) + 55;
    }
    if (maValue <= 10) {
        return 6 * (maValue - 9) + 62;
    }
    if (maValue <= 20) {
        return 3 * (maValue - 11) + 71;
    }
    if (maValue <= 22) {
        return 4 * (maValue - 21) + 105;
    }
    if (maValue <= 24) {
        return 4 * (maValue - 23) + 115;
    }

    return 0;
};

const calcSiValue = (siValue) => {
    if (siValue <= 6) {
        return 4 * (siValue) + 35;
    }
    if (siValue <= 8) {
        return 4 * (siValue - 7) + 64;
    }
    if (siValue <= 10) {
        return 4 * (siValue - 9) + 73;
    }
    if (siValue <= 12) {
        return 4 * (siValue - 11) + 82;
    }

    return 0;
};

const calc = (ids, answers) => {
    return trueCount(ids.trueIds, answers) + falseCount(ids.falseIds, answers);
};

const falseCount = (ids, answers) => {
    let count = 0;
    ids.forEach((item) => {
        if (!answers[item]) {
            count++
        }
    });
    return count;
};

const trueCount = (ids, answers) => {
    let count = 0;
    ids.forEach((item) => {
        if (answers[item]) {
            count++
        }
    });
    return count;
};